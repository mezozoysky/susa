if (DEFINED FIND_CXXTEST_CMAKE_INCLUDED)
    return()
endif (DEFINED FIND_CXXTEST_CMAKE_INCLUDED)
set(FIND_CXXTEST_CMAKE_INCLUDED true)

find_program(CXXTESTGEN
    NAMES   cxxtestgen
    PATHS   "${CXXTEST_PATH}/bin"
            "/usr/local/bin"
            "/usr/bin"
            "${PROJECT_SOURCE_DIR}/cxxtest/bin"
            "${PROJECT_SOURCE_DIR}/lib/cxxtest/bin"
            "${PROJECT_BINARY_DIR}/cxxtest/bin"
            "${PROJECT_BINARY_DIR}/lib/cxxtest/bin"
          )

find_path(CXXTEST_INCLUDES
    NAMES   "cxxtest/TestSuite.h"
    PATHS   "${CXXTEST_PATH}"
            "/usr/local/include"
            "/usr/include"
            "${PROJECT_SOURCE_DIR}/cxxtest/"
            "${PROJECT_SOURCE_DIR}/lib/cxxtest/"
          )

if(NOT CXXTESTGEN)
    message(FATAL_ERROR "Unable to find 'cxxtestgen'")
    set(CXXTEST_FOUND false)
elseif(NOT CXXTEST_INCLUDES)
    set(CXXTEST_FOUND false)
else(NOT CXXTESTGEN)
    set(CXXTEST_FOUND true)
    set(CXXTEST_ROOT ${CXXTEST_INCLUDES})
endif(NOT CXXTESTGEN)

set(CXXTEST_CMAKE_MODULES_PATH "${CMAKE_CURRENT_LIST_DIR}")
include("${CXXTEST_CMAKE_MODULES_PATH}/CxxTest.cmake")

