//  susa
//
//  susa - Copyright (C) 2017 Stanislav Demyanovich
//
//  This software is provided 'as-is', without any express or
//  implied warranty. In no event will the authors be held
//  liable for any damages arising from the use of this software.
//
//  Permission is granted to anyone to use this software for any purpose,
//  including commercial applications, and to alter it and redistribute
//  it freely, subject to the following restrictions:
//
//  1.  The origin of this software must not be misrepresented;
//      you must not claim that you wrote the original software.
//      If you use this software in a product, an acknowledgment
//      in the product documentation would be appreciated but
//      is not required.
//
//  2.  Altered source versions must be plainly marked as such,
//      and must not be misrepresented as being the original software.
//
//  3.  This notice may not be removed or altered from any
//      source distribution.



/// \file
/// \brief This file provides id type
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/lipbng license

#include <cstdint>
#include <limits>
#include <cassert>

#ifndef SUSA__ID_HPP
#define SUSA__ID_HPP

namespace susa
{

/// \brief Template wrapper around integer and it's parts
///
/// \details (N)-bit combination of two (N/2)-bit numbers, first for the index and second for the 'mark' or 'version',
/// provide the concept of 'index validity'. Valid versions starts with 1
template<typename id_full_t = std::uint64_t, typename id_half_t = std::uint32_t>
class id
{
public:
    static const std::uintptr_t FULL_SIZE_BITS { sizeof ( id_full_t ) * 8 };
    static const std::uintptr_t HALF_SIZE_BITS { sizeof ( id_half_t ) * 8 };

    /// \brief Default constructor; constructs invalid id
    inline id() noexcept
    : m_id { 0 }
    {
    }

    /// \brief Constructor from index and version
    inline id( id_half_t index, id_half_t version ) noexcept
    : m_id { (id_full_t)index | (id_full_t)( version == 0 ? 1 : version ) << HALF_SIZE_BITS }
    {
        assert(
            version != 0
            && "Valid susa::id versions start with 1, not 0."
            && "If you want to create invalid id, please, use default constructor of constructor from full size number."
        );
    }

    /// \brief Constructor from a complete full size id number
    inline explicit id( id_full_t id ) noexcept
    : m_id { id }
    {
    }

    inline bool operator == ( const id<id_full_t, id_half_t>& rhs ) const noexcept
    {
        return ( m_id == rhs.m_id );
    }

    bool operator != ( const id<id_full_t, id_half_t>& rhs ) const noexcept
    {
        return ( ! ( *this == rhs ) );
    }

    /// \brief Gets index
    inline id_half_t get_index() const noexcept
    {
        return ( m_id & (id_half_t)( std::numeric_limits<id_half_t>::max() ) );
    }

    /// \brief Gets version
    inline id_half_t get_version() const noexcept
    {
        return ( m_id >> (id_half_t)( HALF_SIZE_BITS ) );
    }

    /// \brief Gets actual id
    inline id_full_t get_id() const noexcept
    {
        return ( m_id );
    }

    inline bool is_valid() const noexcept
    {
        return ( m_id != 0 );
    }

    inline void invalidate() const noexcept
    {
        m_id = 0;
    }

private:
    id_full_t m_id;
};

} //namespace susa

#endif /* end of include guard: SUSA__ID_HPP */

