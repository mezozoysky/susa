// susa
//
// susa - Copyright (C) 2016 Stanislav Demyanovich
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.

/// \file
/// \brief id unit-tests
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/libpng license


#pragma once

#include <cxxtest/TestSuite.h>
#include <susa/id.hpp>

using susa::id;


class id_test_suite
: public CxxTest::TestSuite
{
private:
    static const id<> invalid;

public:
    void test_invalid_id( void )
    {
        TS_ASSERT( ! invalid.is_valid() );

        id<> id {};
        TS_ASSERT( ! id.is_valid() );
        TS_ASSERT_EQUALS( id, invalid );

        TS_ASSERT_EQUALS( id.get_id(), 0 );
        TS_ASSERT_EQUALS( id.get_index(), 0 );
        TS_ASSERT_EQUALS( id.get_version(), 0 );
    }

    void test_valid_id( void )
    {
        id<> id { 1, 1 };

        TS_ASSERT( id.is_valid() );
        TS_ASSERT_DIFFERS( id, invalid );

        TS_ASSERT_EQUALS( id.get_index(), 1 );
        TS_ASSERT_EQUALS( id.get_version(), 1 );
        TS_ASSERT( id.get_id() > 0 );
    }
};

const id<> id_test_suite::invalid {};
