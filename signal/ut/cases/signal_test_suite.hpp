// susa
//
// susa - Copyright (C) 2016 Stanislav Demyanovich
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.

/// \file
/// \brief signal unit-tests
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/libpng license


#pragma once

#include <cxxtest/TestSuite.h>
#include <susa/signal.hpp>

using susa::signal;

class signal_test_suite
: public CxxTest::TestSuite
{
protected:
	static int m_void_signal_call_count;
	static void on_void_signal()
	{
        ++m_void_signal_call_count;
	}

public:
	void test_static_method_delegate_with_no_params( void )
	{
		m_void_signal_call_count = 0;

		signal<> sig1 {};

		auto connection( sig1.connect( std::bind( &signal_test_suite::on_void_signal ) ) );

		TS_ASSERT_EQUALS( m_void_signal_call_count, 0 );
		sig1();
		TS_ASSERT_EQUALS( m_void_signal_call_count, 1 );
		sig1();
		sig1();
		TS_ASSERT_EQUALS( m_void_signal_call_count, 3 );

		sig1.disconnect( connection );
		sig1();
		sig1();
		TS_ASSERT_EQUALS( m_void_signal_call_count, 3 );
	}

	void test_method_delegate_with_int_param( void )
	{
		struct observer
		{
			void on_signal1( int i )
			{
				sig1_sum += i;
			}


			int sig1_sum { 0 };
		};
		signal< int > sig1 {};
		observer oer {};

		auto connection( sig1.connect( std::bind( &observer::on_signal1, &oer, std::placeholders::_1 ) ) );

		sig1( 8 );
		sig1( 22 );

		TS_ASSERT_EQUALS( oer.sig1_sum, 30 );

		sig1.disconnect( connection );
		sig1( 5 );

		TS_ASSERT_EQUALS( oer.sig1_sum, 30 );
	}
};

int signal_test_suite::m_void_signal_call_count = 0;
