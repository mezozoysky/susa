// susa
//
// susa - Copyright (C) 2017 Stanislav Demyanovich
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.

/// \file
/// \brief signal template
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/libpng license

#ifndef SUSA__SIGNAL_HPP
#define SUSA__SIGNAL_HPP

#include <functional>
#include <list>

namespace susa
{

template< typename... signature_t >
class signal
{
public:
	using delegate_t = std::function< void( signature_t... ) >;
	using delegate_list_t = std::list< delegate_t >;

	using connection = typename delegate_list_t::iterator;

	connection connect( const delegate_t& delegate ) noexcept
	{
		m_delegates.push_front( delegate );
		return ( m_delegates.begin() );
	}

	void disconnect( connection& connection ) noexcept
	{
		if ( connection != m_delegates.end() )
		{
			m_delegates.erase( connection );
			connection = m_delegates.end();
		}
	}

	void disconnect_all() noexcept
	{
		while ( !m_delegates.empty() )
		{
			disconnect( m_delegates.back() );
		}
	}

	template< typename... args_t >
	void operator()( args_t&&... args )
	{
		for ( const auto& delegate_t : m_delegates )
		{
			delegate_t( std::forward< args_t >( args )... );
		}
	}

private:
	delegate_list_t m_delegates;
};

} //namespace susa

#endif // SUSA__SIGNAL_HPP
