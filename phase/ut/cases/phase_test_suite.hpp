// susa
//
// susa - Copyright (C) 2016 Stanislav Demyanovich
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.

/// \file
/// \brief Signal unit-tests
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/libpng license


#pragma once

#include <cxxtest/TestSuite.h>
#include <susa/phase/switcher.hpp>
#include <iostream>

using susa::phase::switcher;

class test_switcher
: public switcher<>
{
public:
    test_switcher() = default;
    virtual ~test_switcher() = default;

    virtual void on_phase_leave()
    {
        std::cout << "Leaving phase " << curr_phase() << std::endl;
    }

    virtual void on_phase_enter()
    {
        std::cout << "Enter phase " << curr_phase() << std::endl;
    }

    virtual void on_before_iteration_start() // or on_after_iteration_end() if you like :)
    {
        apply_switch();
    }
};

class phase_test_suite : public CxxTest::TestSuite
{
protected:

public:
	void test_something( void )
	{
        test_switcher sw;
		TS_ASSERT_EQUALS( sw.curr_phase(), test_switcher::PHASE_NULL );
        sw.switch_to( 8 );
        TS_ASSERT_EQUALS( sw.curr_phase(), test_switcher::PHASE_NULL );
        sw.on_before_iteration_start(); // simulation frame, gui update call, etc
        TS_ASSERT_EQUALS( sw.curr_phase(), 8 );
        sw.switch_to( 9 );
        TS_ASSERT_EQUALS( sw.curr_phase(), 8 );
        sw.on_before_iteration_start(); // one more frame
        TS_ASSERT_EQUALS( sw.curr_phase(), 9 );
	}

// 	void test_something_other( void )
// 	{
// 		TS_ASSERT_EQUALS( 30, 30 );
// 	}
};
