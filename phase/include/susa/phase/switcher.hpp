// susa
//
// susa - Copyright (C) 2016 Stanislav Demyanovich
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.

/// \file
/// \brief Phase switcher template
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/libpng license

#ifndef SUSA__PHASE_SWITCHER_HPP
#define SUSA__PHASE_SWITCHER_HPP

#include <cassert>

namespace susa
{
namespace phase
{

template<typename phase_id_t = unsigned, phase_id_t phase_id_null_v = 0>
class switcher
{
public:
    using phase_id = phase_id_t;

    static const phase_id PHASE_NULL = phase_id_null_v;

public:
    switcher()
        : m_curr_phase( PHASE_NULL )
        , m_next_phase( PHASE_NULL )
        , m_switch_requested( false )
    {
    }

    virtual ~switcher() noexcept = default;

    void switch_to( const phase_id& phase );
    inline const phase_id& curr_phase() const noexcept { return ( m_curr_phase ); }

protected:
    void apply_switch();

    virtual void on_phase_leave() = 0;
    virtual void on_phase_enter() = 0;

private:


private:
    phase_id m_curr_phase;
    phase_id m_next_phase;
    bool m_switch_requested;
};

template<typename phase_id_t, phase_id_t phase_id_null_v>
void switcher<phase_id_t, phase_id_null_v>::switch_to( const switcher::phase_id& phase )
{
    m_next_phase = phase;
    m_switch_requested = true;
}

template<typename phase_id_t, phase_id_t phase_id_null_v>
void switcher<phase_id_t, phase_id_null_v>::apply_switch()
{
    if ( m_switch_requested )
    {
        assert( m_next_phase != PHASE_NULL );

        if ( m_curr_phase != PHASE_NULL )
        {
            on_phase_leave();
        }

        m_curr_phase = m_next_phase;
        m_next_phase = PHASE_NULL;
        on_phase_enter();

        m_switch_requested = false;
    }
}

} // namespace phase
} // namespace susa

#endif // SUSA__PHASE_SWITCHER_HPP
