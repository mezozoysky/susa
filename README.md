# susa - some useful set of abstractions #

Where 'useful' means 'usefule for me', but who knows..

*susa* is a couple of independent header-only template libraries, it's written in C++

## Status ##
Just starting

## License ##
*susa* is released under the terms of zlib/png license;
see full license text in LICENSE.md file or at https://opensource.org/licenses/Zlib

## Build ##
Nothing to build

## Install ##
Nothing to install

## Documentation ##
Nothing to document
