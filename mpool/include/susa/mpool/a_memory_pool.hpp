// susa
//
// susa - Copyright (C) 2017 Stanislav Demyanovich
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.


/// \file
/// \brief This file provides a_memory_pool - the abstract base for memory pools
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/libpng license

#ifndef SUSA__MPOOL_A_MEMORY_POOL_HPP
#define SUSA__MPOOL_A_MEMORY_POOL_HPP

#include <cstdint>

namespace susa
{
namespace mpool
{

/// \brief Abstract base for memory pools
class a_memory_pool
{
    a_memory_pool( const a_memory_pool& ) = delete;
    a_memory_pool& operator= ( const a_memory_pool& ) = delete;

public:
    a_memory_pool() = default;
    virtual ~a_memory_pool() = default;

    virtual std::uint32_t get_size() const noexcept = 0;
    virtual std::uint32_t get_capacity() const noexcept = 0;
    virtual std::uint32_t get_chunks_number() const noexcept = 0;
    virtual std::uint32_t get_chunk_capacity() const noexcept = 0;
    virtual std::uint32_t get_free_size() const noexcept = 0;
    virtual std::size_t get_element_size() const noexcept = 0;

    virtual void clear() noexcept = 0;
    virtual void reserve( std::uint32_t cap ) noexcept = 0;

    virtual bool is_alive( std::uint32_t index ) const noexcept = 0;
};

} //namespace mpool
} //namespace susa

#endif /* end of include guard: SUSA__MPOOL_A_MEMORY_POOL_HPP */
