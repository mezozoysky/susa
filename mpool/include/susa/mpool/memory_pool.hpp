// susa
//
// susa - Copyright (C) 2017 Stanislav Demyanovich
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.


/// \file
/// \brief This file provides memory_pool template
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/libpng license

#ifndef SUSA__MPOOL_MEMORY_POOL_HPP
#define SUSA__MPOOL_MEMORY_POOL_HPP

#include "a_memory_pool.hpp"
#include <susa/id.hpp>
#include <vector>
#include <cassert>
#include <type_traits>
#include <functional>
#include <algorithm>

namespace susa
{
namespace mpool
{

/// \brief Pool for contiguous managed memory allocation
/// \tparam value_t The data type to manage memory for
/// \tparam chunk_size_v Number of \em value_t per memory chunk
///
/// Usage example:
/// \code{.cpp}
/// #include <susa/mpool/memory_pool.hpp>
/// #include <susa/id.hpp>
/// #include <cassert>
/// using susa::mpool::memory_pool;
/// using susa::id;
/// struct some
/// {
///     some( int i, const std::string& s )
///     : idata{ i }
///     , sdata{ s }
///     {
///     }
///     int idata{0};
///     std::string sdata{""};
/// };
/// memory_pool< some, 32 > pool;
/// id<> id{ pool.alloc( 8, "aubergine" ) };
/// assert( pool.is_id_valid( id ) && "Bug in memory_pool!" );
/// \endcode
template < typename value_t, std::uint32_t chunk_size_v = 256 >
class memory_pool
: public a_memory_pool
{
public:

    /// \brief Wrap-type for managing value type
    using value_type_t = value_t;
    /// \brief Wrap-type for underlaying aligned_storage template
    using aligned_type_t = typename std::aligned_storage< sizeof( value_type_t ), alignof( value_type_t ) >::type;

    /// \brief Represents one chunk of allocated memory
    struct chunk
    {
        aligned_type_t data[ chunk_size_v ]; ///< Array of aligned memory
    };

    /// \brief Default constructor
    memory_pool() noexcept
    {
    }

    /// \brief Virtual destructor for further inheritance
    virtual ~memory_pool()
    {
        clear();
    }

    /// \brief Allocate one element with constructor \em args
    template< typename... args_t >
    id<> alloc( args_t&&... args )
    {
        std::uint32_t index;

        if ( m_free_indices.empty() )
        {
            index = m_versions.size();

            if ( index == m_capacity )
            {
                add_chunk();
            }

            m_versions.push_back( 1 );
        }
        else
        {
            index = m_free_indices.back();
            m_free_indices.pop_back();
        }

        std::size_t chunk_index{ index / chunk_size_v };
        std::size_t element_index{ index % chunk_size_v };
        new( m_chunks[ chunk_index ].data + element_index ) value_type_t( std::forward< args_t >( args )... );

        id<> id{ index, m_versions[ index ] };
        return ( id );
    }

    /// \brief Free memory block by given id
    void free( id<> id )
    {
        if ( !is_id_valid( id ) )
        {
            return;
        }
        free( id.get_index() );
    }

    /// \brief Gets pointer to memory block
    value_type_t* get_ptr( id<> id ) const noexcept
    {
        if ( !is_id_valid( id ) )
        {
            return ( nullptr );
        }
        std::uint32_t index{ id.get_index() };
        assert( index < m_versions.size() && "Bad index!" );
        std::size_t chunk_index{ index / chunk_size_v };
        std::size_t element_index{ index % chunk_size_v };
        return ( const_cast< value_type_t* >( reinterpret_cast< const value_type_t* >( m_chunks[ chunk_index ].data + element_index ) ) ); //fuck!
    }

    /// \brief Gets const reference to memory block
    const value_type_t& operator[]( id<> id ) const
    {
        if ( !is_id_valid( id ) )
        {
            throw std::range_error( "memory_pool element access violation" );
        }
        std::uint32_t index{ id.get_index() };
        assert( index < m_versions.size() && "Bad index!" );
        std::uint32_t chunk_index{ index / chunk_size_v };
        std::uint32_t element_index{ index % chunk_size_v };
        return ( *reinterpret_cast< const value_type_t* >( m_chunks[ chunk_index ].data + element_index ) );
    }

    /// \brief Gets reference to memory block
    value_type_t& operator[]( id<> id )
    {
        if ( !is_id_valid( id ) )
        {
            throw std::range_error( "memory_pool element access violation" );
        }
        std::uint32_t index{ id.get_index() };
        assert( index < m_versions.size() && "Bad index!" );
        std::uint32_t chunk_index{ index / chunk_size_v };
        std::uint32_t element_index{ index % chunk_size_v };
        return ( *reinterpret_cast< value_type_t* >( m_chunks[ chunk_index ].data + element_index ) );
    }

    /// \brief Pool size
    virtual std::uint32_t get_size() const noexcept final
    {
        return ( m_versions.size() );
    }

    /// \brief Pool capacity (memory footprint)
    virtual std::uint32_t get_capacity() const noexcept final
    {
        return ( m_capacity );
    }

    /// \brief Number of chunks used
    virtual std::uint32_t get_chunks_number() const noexcept final
    {
        return ( m_chunks.size() );
    }

    /// \brief Getter for chunk capacity
    virtual std::uint32_t get_chunk_capacity() const noexcept final
    {
        return ( chunk_size_v );
    }

    /// \brief Number of free elements contained
    virtual std::uint32_t get_free_size() const noexcept final
    {
        return ( m_free_indices.size() );
    }

    /// \brief Memory block size in bytes
    virtual std::size_t get_element_size() const noexcept final
    {
        return ( sizeof( value_type_t ) );
    }

    /// \brief Calls destructor for each alive element, deallocates the chunks
    virtual void clear() noexcept final
    {
        for ( std::uint32_t i = 0; i < m_versions.size(); ++i )
        {
            if ( !is_alive( i ) )
            {
                continue;
            }
            free( i );
        }

        while ( !m_chunks.empty() )
        {
            m_chunks.pop_back();
        }
    }

    /// \brief Reserves capacity >= \em cap
    virtual void reserve( std::uint32_t cap ) noexcept final
    {
        while ( m_capacity < cap )
        {
            add_chunk();
        }
    }

    /// \brief Test if id valid
    virtual bool is_id_valid( id<> id  ) const noexcept final
    {
        std::uint32_t index{ id.get_index() };
        if ( index < m_versions.size() && m_versions[ index ] == id.get_version() )
        {
            return ( true );
        }
        return ( false );
    }

    /// \brief Calls given function for every element
    void for_each( std::function< void( value_type_t& value ) > func )
    {
        for ( std::uint32_t index{ 0 }; index < get_size(); ++index )
        {
            if ( !is_alive( index ) ) continue;
            std::uint32_t chunk_index{ index / chunk_size_v };
            std::uint32_t element_index{ index % chunk_size_v };
            func ( *reinterpret_cast< value_type_t* >( m_chunks[ chunk_index ].data + element_index ) );
        }
    }

private:

    /// \brief Adds new chunk of memory onto the field
    void add_chunk()
    {
        m_chunks.emplace_back();
        m_capacity += chunk_size_v;
        m_versions.reserve( m_capacity );
    }

    /// \brief Mark the memory block as free
    void free( std::uint32_t index )
    {
        std::size_t chunk_index{ index / chunk_size_v };
        std::size_t element_index{ index % chunk_size_v };
        reinterpret_cast< const value_type_t* >( m_chunks[ chunk_index ].data + element_index )->~value_type_t();
        m_versions[ index ]++;

        auto it = std::lower_bound( m_free_indices.begin(), m_free_indices.end(), index, std::greater< std::size_t >() );
        m_free_indices.insert( it, index );
    }

    /// \brief Test if the index not in the free-list
    virtual bool is_alive( std::uint32_t index ) const noexcept final
    {
        assert( index < m_versions.size() && "Bad index!" );
        auto it( std::find_if( m_free_indices.begin(), m_free_indices.end(), [index]( std::uint32_t dead ) { return ( dead == index ); } ) );
        return ( it == m_free_indices.end() );
    }

private:
    std::uint32_t m_capacity{ 0 }; ///< Current pool capacity

    std::vector< chunk > m_chunks; ///< Actual chunks of our managed memory.
    std::vector< std::uint32_t > m_versions; ///< Vector of current versions for memory blocks.
    std::vector< std::uint32_t > m_free_indices; ///< Vector of free-marked elements' indexes. Keeping sorted.
};


} //namespace mpool
} //namespace susa

#endif /* end of include guard: SUSA__MPOOL_MEMORY_POOL_HPP */
