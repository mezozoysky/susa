set( MODULE_UT_TGT "${MODULE_NAME}-ut" )

set( MODULE_UT_ROOT ${CMAKE_CURRENT_SOURCE_DIR} )

set( MODULE_UT_HEADERS )
set( MODULE_UT_SOURCES )

include_directories( "${MODULE_ROOT}/include" )
set( MODULE_UT_CXXTEST_CASES_ROOT "${MODULE_UT_ROOT}/cases" )

# ===================================
# ==== ${MODULE_NAME}/ut group ====
# ===================================
set(GROUP_HEADERS
	# ${MODULE_UT_ROOT}/Some.hpp
)
set(GROUP_SOURCES
	# ${MODULE_UT_ROOT}/Some.cpp
)
source_group( "${MODULE_NAME}\\ut" FILES ${GROUP_HEADERS} ${GROUP_SOURCES} )
set( MODULE_UT_HEADERS ${MODULE_UT_HEADERS} ${GROUP_HEADERS} )
set( MODULE_UT_HEADERS ${MODULE_UT_HEADERS} ${GROUP_SOURCES} )

set( MODULE_UT_CXXTEST_CASES_HEADERS )
# =========================================
# ==== ${MODULE_NAME}/ut/cases group ====
# =========================================
set(GROUP_HEADERS
	${MODULE_UT_CXXTEST_CASES_ROOT}/mpool_test_suite.hpp
)
source_group( "${MODULE_NAME}\\ut\\cases" FILES ${GROUP_HEADERS} )
set( MODULE_UT_CXXTEST_CASES_HEADERS ${GROUP_HEADERS} )


#
# Generate CxxTest cases sources
#

set( MODULE_UT_CXXTEST_CASES_SOURCES )
foreach( header ${MODULE_UT_CXXTEST_CASES_HEADERS} )
	set( UT_SOURCE_FILE )
	ay_cxxtest_generate_part( UT_SOURCE_FILE "${header}" "${MODULE_NAME}" )
	set( MODULE_UT_CXXTEST_CASES_SOURCES ${MODULE_UT_CXXTEST_CASES_SOURCES} ${UT_SOURCE_FILE} )
endforeach()

set( UT_ROOT_SOURCE_FILE )
ay_cxxtest_generate_root( UT_ROOT_SOURCE_FILE "${MODULE_NAME}" )
set( MODULE_UT_CXXTEST_CASES_SOURCES ${MODULE_UT_CXXTEST_CASES_SOURCES} ${UT_ROOT_SOURCE_FILE} )

message( STATUS "UT HEADERS: ${MODULE_UT_CXXTEST_CASES_HEADERS}" )
message( STATUS "UT SOURCES: ${MODULE_UT_CXXTEST_CASES_SOURCES}" )

#
# Build
#
add_executable( ${MODULE_UT_TGT} WIN32
    ${MODULE_HEADERS}
	${MODULE_UT_CXXTEST_CASES_HEADERS} ${MODULE_UT_HEADERS}
	${MODULE_UT_CXXTEST_CASES_SOURCES} ${MODULE_UT_SOURCES}
)
set_target_properties( ${MODULE_UT_TGT} PROPERTIES COMPILE_FLAGS "-Wno-effc++" )
target_link_libraries( ${MODULE_UT_TGT}
#     ${MODULE_NAME}
    # <== custom libraries
    # custom libraries ==>
)

install(
	TARGETS ${MODULE_UT_TGT}
	RUNTIME DESTINATION share/${CMAKE_PROJECT_NAME}/bin
)
