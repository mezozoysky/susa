// susa
//
// susa - Copyright (C) 2016 Stanislav Demyanovich
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.

/// \file
/// \brief Memory pool unit-tests
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/libpng license


#pragma once

#include <cxxtest/TestSuite.h>
#include <susa/mpool/memory_pool.hpp>
#include <iostream>
#include <string>

using susa::mpool::memory_pool;
using susa::id;

typedef std::string value_type;

struct some
{
    some( int i, const std::string& s )
    : idata( i )
    , sdata( s )
    {
    }
    int idata{0};
    std::string sdata{""};
};

class mpool_test_suite : public CxxTest::TestSuite
{
public:
	void test_alloc_id( void )
	{
        memory_pool<some, 32> pool;
        id<> id{ pool.alloc( 8, "aubergine" ) };
        TS_ASSERT_EQUALS( pool.is_id_valid( id ), true );
	}

	void test_alloc_correctness( void )
    {
        memory_pool<value_type, 2> pool;
        TS_ASSERT_EQUALS( pool.get_capacity(), 0 );
        TS_ASSERT_EQUALS( pool.get_size(), 0 );

        id<> id{ pool.alloc( "zero" ) };
        TS_ASSERT( pool.is_id_valid( id ) );
        TS_ASSERT_EQUALS( pool.get_capacity(), 2 );
        TS_ASSERT_EQUALS( pool.get_size(), 1 );
        TS_ASSERT_EQUALS( *pool.get_ptr( id ), std::string( "zero" ) );

        auto id1( pool.alloc( "one" ) );
        TS_ASSERT( pool.is_id_valid( id1 ) );
        TS_ASSERT_EQUALS( pool.get_capacity(), 2 );
        TS_ASSERT_EQUALS( pool.get_size(), 2 );
        TS_ASSERT_EQUALS( pool[ id1 ], "one" );

        auto id2( pool.alloc( "two" ) );
        TS_ASSERT( pool.is_id_valid( id2 ) );
        TS_ASSERT_EQUALS( pool.get_capacity(), 4 );
        TS_ASSERT_EQUALS( pool.get_size(), 3 );
        TS_ASSERT_EQUALS( pool[ id2 ], "two" );

        auto id3( pool.alloc( "three" ) );
        TS_ASSERT( pool.is_id_valid( id3 ) );
        TS_ASSERT_EQUALS( pool.get_capacity(), 4 );
        TS_ASSERT_EQUALS( pool.get_size(), 4 );
        TS_ASSERT_EQUALS( pool[ id3 ], "three" );

        id = pool.alloc( "four" );
        TS_ASSERT( pool.is_id_valid( id ) );
        TS_ASSERT_EQUALS( pool.get_capacity(), 6 );
        TS_ASSERT_EQUALS( pool.get_size(), 5 );
        TS_ASSERT_EQUALS( pool[ id ], "four" );

        pool.free( id1 );
        pool.free( id3 );

        id = pool.alloc( "nova" );
        TS_ASSERT( pool.is_id_valid( id ) );
        TS_ASSERT_EQUALS( id.get_index(), 1 );

        id = pool.alloc( "super nova" );
        TS_ASSERT_EQUALS( id.get_index(), 3 );
    }
};
