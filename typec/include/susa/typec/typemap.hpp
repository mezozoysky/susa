// susa
//
// susa - Copyright (C) 2016 Stanislav Demyanovich
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.

/// \file
/// \brief typemap template
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/libpng license

#ifndef SUSA__TYPEC_TYPEMAP_HPP
#define SUSA__TYPEC_TYPEMAP_HPP

#include <cstdint>
#include <atomic>
#include <map>
#include <algorithm>
#include <functional>

namespace susa
{
namespace typec
{

template <typename value_type_t, typename index_type_t = std::size_t>
class typemap
{

private:
	using map_impl = std::map< index_type_t, value_type_t >;

public:
	using iterator = typename map_impl::iterator;
	using const_iterator = typename map_impl::const_iterator;
    using index_type = index_type_t;
	using value_type = typename map_impl::value_type;

	template< typename type_key >
	inline void insert( const value_type_t& value ) noexcept
	{
		m_map.insert( std::make_pair( get_unique_type_id< type_key >(), value ) );
	}

	template< typename type_key >
	inline value_type_t& at()
	{
		return ( m_map.at( get_unique_type_id< type_key >() ) );
	}

	template< typename type_key >
	inline const value_type_t& at() const
	{
		return ( m_map.at( get_unique_type_id< type_key >() ) );
	}

	template< typename type_key >
	inline std::size_t count() const noexcept
	{
		return ( m_map.count( get_unique_type_id< type_key >() ) );
	}

	template< typename type_key >
	inline std::size_t erase() noexcept
	{
		return ( m_map.erase( get_unique_type_id< type_key >() ) );
	}

	inline iterator erase( const_iterator pos )
	{
		return ( m_map.erase( pos ) );
	}

	inline iterator erase( const_iterator first, const_iterator last )
	{
		return ( m_map.erase( first, last ) );
	}

	inline void forEach( const std::function< void( value_type_t& ) >& func )
	{
		std::for_each( m_map.begin(), m_map.end(), func );
	}

	inline const_iterator begin() const noexcept
	{
		return ( m_map.begin() );
	}

	inline const_iterator end() const noexcept
	{
		return ( m_map.end() );
	}

	inline iterator begin() noexcept
	{
		return ( m_map.begin() );
	}

	inline iterator end() noexcept
	{
		return ( m_map.end() );
	}

	template < typename type_key >
	inline iterator find() noexcept
	{
		return ( m_map.find( get_unique_type_id< type_key >() ) );
	}

	template < typename type_key >
	inline const_iterator find() const noexcept
	{
		return ( m_map.find( get_unique_type_id< type_key >() ) );
	}

	inline void clear() noexcept
	{
		m_map.clear();
	}

	inline std::size_t size() const noexcept
	{
		return ( m_map.size() );
	}

private:

	template< typename type_key >
	inline static int get_unique_type_id() noexcept
	{
		static const std::size_t id = m_next_type_id++;
		return id;
	}

private:
	static std::atomic_size_t m_next_type_id;
	map_impl m_map;
};

template<typename value_type_t, typename index_type_t>
std::atomic_size_t typemap<value_type_t, index_type_t>::m_next_type_id { 0 };


} //namespace typec
} //namespace susa

#endif // SUSA__TYPEC_TYPEMAP_HPP
