// susa
//
// susa - Copyright (C) 2016 Stanislav Demyanovich
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.

/// \file
/// \brief typemap unit-tests
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2017
/// \copyright susa is released under the terms of zlib/png license


#pragma once

#include <cxxtest/TestSuite.h>
#include <susa/typec/typemap.hpp>
#include <string>

using susa::typec::typemap;

class typemap_test_suite : public CxxTest::TestSuite
{
public:
	void test_insert_correctness( void )
	{
		using value_type = std::string;
		typemap< value_type > map;

		map.insert< int >( "integer type" );
		TS_ASSERT_EQUALS( map.find< int >()->second, "integer type" );

		map.insert< bool >( "bool type" );
		TS_ASSERT_EQUALS( map.at< bool >(), "bool type" );

		map.insert< float >( "float type" );
		TS_ASSERT_EQUALS( map.count<float>(), 1 );
		map.insert< float >( "float type again" );
		TS_ASSERT_EQUALS( map.count<float>(), 1 );
		TS_ASSERT_EQUALS( map.at< float >(), "float type" );
	}
};
